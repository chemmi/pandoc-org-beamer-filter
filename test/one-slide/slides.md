# Slide

::: {.example}
## example block title
text in example block
:::

::: {.alert}
## alert block title {.alert}
text in alert block
:::

text outside of blocks

::: {.block}
## block title
text in normal block
:::

## block title 2
more text in normal block
