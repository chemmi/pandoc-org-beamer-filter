#!/bin/bash

# TC: one slide
tc=one-slide
echo "-------------- Test case: $tc"

pandoc -t beamer $tc/slides.org --slide-level=1 --filter ../pandoc-org-beamer-filter.py > $tc/slides.org.tex

if diff $tc/slides.expected.tex $tc/slides.org.tex
then
   echo "-------------- > PASSED"
fi
echo "-------------- End test case: $tc"
echo ""

# TC: multiple slides
tc=multiple-slides
echo "-------------- Test case: $tc"

pandoc -t beamer $tc/slides.org --slide-level=1 --filter ../pandoc-org-beamer-filter.py > $tc/slides.org.tex

if diff $tc/slides.expected.tex $tc/slides.org.tex
then
   echo "-------------- > PASSED"
fi
echo "-------------- End test case: $tc"
echo ""
