#!/usr/bin/python3

import os
from panflute import *
import sys

SLIDE_LEVEL = 1

def main(doc=None):
    return run_filters((put_headers_in_divs, reformat), doc=doc)

attr_to_class = {
    "example": "example",
    "alertblock": "alert",
    "block": "block",
    }

def put_headers_in_divs(elem, doc):
    if isinstance(elem, Doc):
        blocks = list(elem.content)
        new_blocks = list()
        popped = list()
        while last := blocks.pop():
            if isinstance(last, Header):
                if last.level > SLIDE_LEVEL:
                    new_blocks.insert(0, Div(last, *popped))
                else:
                    new_blocks.insert(0, Div(*popped))
                    new_blocks.insert(0, last)
                popped.clear()
            else:
                popped.insert(0, last)
                    
            if not blocks:
                break
        elem.content = popped + new_blocks
        return elem

def reformat(elem, doc):
    if isinstance(elem, Header):
        
        try:
            if elem.attributes["beamer_env"] == 'ignoreheading':
                return Div()
        except KeyError:
            pass

        
        try:
            elem.classes.append(attr_to_class[elem.attributes["beamer_env"]])
        except KeyError:
            pass
        else:
            maybe_tag = elem.content.pop()
            if not isinstance(maybe_tag, Span) and not 'tag' in maybe_tag.classes:
                elem.content.append(maybe_tag)
            else:
                # Remove tailing space
                elem.content.pop()

        return elem
    
if __name__ == "__main__":
    main()
